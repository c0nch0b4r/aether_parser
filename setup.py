#!/usr/bin/var python
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


try:
    from aether_parser.version import __version__
except ImportError:
    pass

exec(open('aether_parser/version.py').read())


setup(
    name='aether_parser',
    version=__version__,
    description='A utility for parsing FFXIV RR network traffic',
    long_description=readme(),
    long_description_content_type='text/x-rst',
    author='c0nch0b4r',
    author_email='lp1.on.fire@gmail.com',
    packages=[
        'aether_parser'
    ],
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    keywords='ffxiv parser',
    url='https://bitbucket.org/c0nch0b4r/aether_parser',
    download_url='https://bitbucket.org/c0nch0b4r/aether_parser/get/' + __version__ + '.tar.gz',
    project_urls={
        'Source': 'https://bitbucket.org/c0nch0b4r/aether_parser/src'
    },
    python_requires='>=3.6, <4',
    install_requires=[
        'typing',
        'hexdump',
        'scapy'
    ],
    entry_points={
        'console_scripts': ['aether_parser=aether_parser.starter:main'],
    },
    include_package_data=True
)
