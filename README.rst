======
README
======

aether_parser can take a pcap with FFXIV: RR traffic (or listen on a network interface for live traffic) and extract
chat logs. This library was built with two purposes in mind. Firstly, to extract chat data from FFXIV network traffic.
Secondly, to be easily expandable to handle parsing other FFXIV traffic.

It should be noted that because there doesn't seem to be one comprehensive guide to the format of FFXIV RR network
traffic, this library contains a lot of guesswork and many assumptions could be very wrong. If you see something that
you know isn't right, please let me know. Or better yet, put in a pull request.


-----
Usage
-----




~~~~~~~~~~~~
As a Library
~~~~~~~~~~~~


Parsing a pcap
**************

>>> from aether_parser import AetherParser
>>> parser = AetherParser(pcap='ffxiv.pcap')
>>> parser.parse()
>>>

After completion, `parser.message_collection` will have information about FFXIV network messages and `parser.mapper`
will have objects relating to user, chat, and FC information found in the messages. All chat messages detected will be
found in `parser.message_collection.chat_submessages`.

String representations of all chat messages found will also be logged to `logging.INFO`.


Listening on an Interface
*************************

>>> from aether_parser import AetherParser
>>> parser = AetherParser(interface='en0', output_file='ffxiv_chat.log', chat_only=True)
>>> parser.parse()
>>>

The parser will listen on the provided interface and parse and FFXIV traffic it encounters. With the `chat_only` flag,
any non-chat related messages are discarded. A log of chat messages will also be written to the provided `output_file`.

Note that here, `parser.parse()` will continue running until it receives a CTRL+C or is killed.

~~~~~~~~~~~~~~~~~~~~~
From the Command Line
~~~~~~~~~~~~~~~~~~~~~


Listening on an Interface (Windows)
***********************************
::

  $ aether_parser -o chat.log -i 'Ethernet'

This will log all chat traffic to the console and to the file `chat.log`. Run `aether_parser --help` for more options.

Note: To get the name of the interface, the easiest method on Windows is to use `get_if_list()` from scapy.


-----------------
Traffic Structure
-----------------

By way of a quick primer on how FFXIV RR network traffic is formatted, there's essentially three layers in each TCP
packet. Within the library, these layers are the outer 'bundle' which can contain one or more 'messages', which can
itself contain one or more 'submessages'. Here, a 'bundle' is just an abstraction for multiple 'messages' in a single
TCP packet. With one or two odd exceptions, all values are little-endian.

The message seems to always begin with these 16 bytes: 0x5252a041ff5d46e27f2a644d7b99c475
This is followed directly by an 8-byte value which is a timestamp containing the number of milliseconds since unix
epoch. The next 4 bytes are the length of the current message in bytes. Then, there's 2 bytes for the 'connection type',
2 bytes for the submessage count, 1 byte for whether or not the message is 'authenticated', and 1 byte for whether or
not the message is compressed. Following that, there are 6 bytes whose purpose are unknown, followed immediately after
by the body (submessage(s)). If the compression byte is 0x01, then the message body is zlib compressed.

The first 4 bytes of the submessage are the total submessage length, followed by 4 bytes for 'source actor' and 4 for
'target actor'. The next two bytes are the 'segment type' -- most of the time this will be 0x03. If it is not 0x03,
there are two bytes of padding followed by the submessage body. If the segment type IS 0x03, there are 4 bytes of
unknown use, then 2 bytes that seem to denote the type of submessage (i.e. user introduction, chat message, etc.).
After 2 more unknown bytes, there is a 2-byte 'server id', then 4 bytes that are usually (but not always?) used for a
traditional timestamp in seconds since unix epoch (Note that this is *seconds*, not milliseconds like in the message
header). After another 4 unknown bytes, the submessage body begins.

Some submessages share the same format, but it seems that for the most part they each have to be parsed separately.


-----------
Limitations
-----------

This library currently works at the packet level and makes no attempts to follow TCP streams or reassemble message
bundles split across packets. As such, only the beginning few messages of a bundle will be seen and there will be plenty
of partial messages.

This is mostly good enough for parsing chat messages as they always seem to be the first things in their respective
bundles and rarely cross packet boundaries, but in the future this should still be changed.

-----
To-Do
-----

 - Reassemble streams and bundles
 - Handle more message types
 - Make upgrading with major FF versions easier
 - Rewrite the `Traffic Structure` section to be clearer. Use offsets.
 - Parse FC information
 - Parse Fellowship info
 - Better alliance member information

----------------
Acknowledgements
----------------

Apart from lots of trial and error in attempting to determine message structure, the following projects have also been
very useful:

 - https://github.com/SapphireServer/Sapphire
    - Network/CommonNetwork.h
 - http://ffxivclassic.fragmenterworks.com/wiki/index.php/Packet_Headers
 - https://github.com/ravahn/machina
    - Machina.FFXIV/Headers/FFXIMessageHeader.cs
    - Machina.FFXIV/Headers/FFXIVBundleHeader.cs
    - Machina.FFXIV/FFXIVBundleDecoder.cs
 - https://xivapi.com/
