#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import logging
import re
from typing import BinaryIO

from aether_parser.version import __version__
from aether_parser.constants import FILE_MAGIC_TO_TYPE


def get_options() -> dict:
    parser = argparse.ArgumentParser(
        prog='ffxiv_parser',
        description="Parse FFXIV traffic for chat messages",
        fromfile_prefix_chars='@'
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version="%(prog)s {}".format(__version__)
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        dest="debug_logging",
        help="Show debug messages"
    )
    parser.add_argument(
        "-c",
        "--config",
        action="store",
        metavar="FILE",
        dest="config_file",
        help="Config file to read settings from"
    )
    parser.add_argument(
        "-C",
        "--chat-only",
        action="store_true",
        dest="chat_only",
        help="Only store chat-related messages"
    )
    parser.add_argument(
        "-o",
        "--output",
        action="store",
        metavar="FILE",
        dest="output_file",
        help="Output file for text chat log"
    )
    parser.add_argument(
        "-p",
        "--pcap",
        action="store",
        metavar="FILE",
        dest="pcap",
        help="PCAP file to read from instead of an interface"
    )
    parser.add_argument(
        "-i",
        "--interface",
        action="store",
        metavar="INTERFACE",
        dest="interface",
        help="Interface to listen for traffic on"
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        dest="quiet",
        help="Do not produce console output unless critical. Overrides `-d`"
    )
    parser.add_argument(
        "-S",
        "--scapy_pcap",
        action="store_true",
        dest="scapy_pcap",
        help="If reading from a pcap, use Scapy instead of the built-in capture reader. Slower, but more versatile."
    )
    options = vars(parser.parse_args())
    if options['config']:
        from aether_parser.classes import AetherConfig
        options['config'] = AetherConfig.from_json(options['config'])
    if options['debug_logging'] and not options['quiet']:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)-7s] %(message)s')
    elif options['quiet']:
        logging.basicConfig(level=logging.CRITICAL)
    else:
        logging.basicConfig(level=logging.INFO, format='%(message)s')

    del options['debug_logging']

    return options


def get_strings(binary, string_length=5):
    return re.findall(b'[A-Za-z0-9/\\-:.,_$%\'()[\\]<> ]{' + bytes(str(string_length), 'utf-8') + b',}', binary)


def get_file_type(fp: BinaryIO):
    fp.seek(0, 0)
    magic_bytes = fp.read(4)
    if FILE_MAGIC_TO_TYPE[magic_bytes][0] == 'pcap':
        fp.seek(0, 0)
        return FILE_MAGIC_TO_TYPE[magic_bytes]
    elif FILE_MAGIC_TO_TYPE[magic_bytes][0] == 'pcapng':
        fp.seek(4, 1)
        byte_order_magic = fp.read(4)
        if byte_order_magic == b'\x1A\x2B\x3C\x4D':
            endian = '>'
        elif byte_order_magic == b'\x4D\x3C\x2B\x1A':
            endian = '<'
        else:
            raise ValueError(f'Unknown pcapng byte order magic: `{byte_order_magic}`!')
        fp.seek(0, 0)
        return 'pcapng', endian
    else:
        raise ValueError(f'Unknown file magic: `{magic_bytes}`!')
