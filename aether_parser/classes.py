#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import OrderedDict, deque, MutableMapping
from hexdump import hexdump, dehex
from datetime import datetime
from typing import Optional, List, Union, Set, Callable
import struct
import zlib
import logging
import json
import gzip
import base64
from weakref import ref as wr_ref

from scapy.sendrecv import sniff
from scapy.layers.inet import TCP

from aether_parser.constants import *
from aether_parser.utils import get_file_type, get_strings


class AetherItemLimits(object):
    def __init__(
            self,
            mapper: int = 1000,
            bundles: int = 250000,
            messages: int = 500000,
            submessages: int = 1000000,
            chat_submessages: int = 10000,
            invalid_binaries: int = 500,
            broken_bundles: int = 500,
            decompression_errors: int = 500,
            chat_submessages_by_type: int = 1000,
            submessages_by_type: int = 10000,
            strings_by_type: int = 10000
    ):
        self.mapper = mapper
        self.bundles = bundles
        self.messages = messages
        self.submessages = submessages
        self.chat_submessages = chat_submessages
        self.invalid_binaries = invalid_binaries
        self.broken_bundles = broken_bundles
        self.decompression_errors = decompression_errors
        self.chat_submessages_by_type = chat_submessages_by_type
        self.submessages_by_type = submessages_by_type
        self.strings_by_type = strings_by_type


class AetherConfig(object):
    """
    Configuration abstraction for AetherParser.

    Sections:

    item_limits: Limits for storing various types of objects.
    """

    def __init__(
            self,
            config_file: Optional[str] = None,
            item_limits: Optional[dict] = None,
            extract_strings: bool = True
    ):
        self.config_file = config_file
        if item_limits:
            self.item_limits = AetherItemLimits(**item_limits)
        else:
            self.item_limits = AetherItemLimits()
        self.extract_strings = extract_strings

    @classmethod
    def from_json(cls, config_file):
        with open(config_file, 'r') as fp:
            config = json.load(fp=fp)
        config['config_file'] = config_file
        return cls(**config)


class LimitedDict(MutableMapping):
    """
    A restricted-length mutable mapping, with optional default values for missing keys and weakrefs.
    Adapted from: https://stackoverflow.com/a/2438926
    """

    def __init__(
            self,
            maxlen: int = 0,
            default_factory: Optional[Callable] = None,
            weakref: bool = False,
            *args,
            **kwargs
    ):
        self.__maxlen = maxlen
        self.__dict = dict(*args, **kwargs)
        if default_factory and not callable(default_factory):
            raise TypeError('default_factory must be callable')
        self.__default_factory = default_factory
        self.__weakref = weakref
        if self.__maxlen:
            while len(self) > self.__maxlen:
                self.popitem()

    def __repr__(self):
        return repr(self.__dict)

    def __str__(self):
        return str(self.__dict)

    def __iter__(self):
        return iter(self.__dict)

    def __len__(self):
        return len(self.__dict)

    def __getitem__(self, key):
        if key in self.__dict:
            if self.__weakref:
                value = self.__dict[key]()
                if value is None:
                    del self.__dict[key]
                    return None
                return value
            return self.__dict[key]
        else:
            return self.__missing__(key)

    def __delitem__(self, key):
        del self.__dict[key]

    def __setitem__(self, key, value):
        if self.__maxlen:
            if key not in self and len(self) == self.__maxlen:
                self.popitem()
        if self.__weakref:
            self.__dict[key] = wr_ref(value, self._weakref_callback)
        else:
            self.__dict[key] = value

    def __missing__(self, key):
        if self.__default_factory:
            if self.__weakref:
                self.__dict[key] = wr_ref(self.__default_factory(), self._weakref_callback)
                return self.__dict[key]()
            else:
                self.__dict[key] = self.__default_factory()
                return self.__dict[key]
        else:
            raise KeyError(key)

    def __contains__(self, item):
        if self.__weakref:
            for key in self.__dict:
                if item == self.__dict[key]():
                    return True
        return item in self.__dict

    def _weakref_callback(self, ref):
        for key in self.__dict:
            if ref == self.__dict[key]:
                del self.__dict[key]


class AetherDynamicObjectMappings(object):
    def __init__(
            self,
            config: AetherConfig = AetherConfig()
    ):
        self.config = config
        self.item_limit = config.item_limits.mapper
        self.user = {
            'by_id': LimitedDict(self.item_limit, weakref=True),
            'by_username': LimitedDict(self.item_limit, weakref=True)
        }
        self.user_list = deque(maxlen=self.item_limit)
        self.chat = {
            'by_id': LimitedDict(self.item_limit, weakref=True),
            'by_name': LimitedDict(self.item_limit, weakref=True)
        }
        self.chat_list = deque(maxlen=self.item_limit)
        self.fc = {
            'by_id': LimitedDict(self.item_limit, weakref=True),
            'by_tag': LimitedDict(self.item_limit, weakref=True),
            'by_name': LimitedDict(self.item_limit, weakref=True)
        }
        self.fc_list = deque(maxlen=self.item_limit)

    def __str__(self):
        return f'<{self.__class__.__name__} ' \
               f'Users:{len(self.user_list)}|Chats:{len(self.chat_list)}|FCs:{len(self.fc_list)}>'

    def cleanup(self):
        for user in self.user_list:
            if user.user_id not in self.user['by_id']:
                self.user['by_id'][user.user_id] = user
            if user.free_company is not None:
                if user.free_company.name not in self.fc['by_name']:
                    logging.warning('Found a leaf')
                    self.upsert_fc(user.free_company)
                self.fc['by_name'][user.free_company.name].members.add(user)
        for chat in self.chat_list:
            if chat.chat_id not in self.chat['by_id']:
                self.chat['by_id'][chat.chat_id] = chat
        for fc in self.fc_list:
            if fc.fc_id not in self.fc['by_id']:
                self.fc['by_id'][fc.fc_id] = fc
            if fc.tag not in self.fc['by_tag']:
                self.fc['by_tag'][fc.tag] = fc

    def upsert_user(self, user: 'AetherUser') -> 'AetherUser':
        if user.username in self.user['by_username']:
            if user is self.user['by_username'][user.username]:
                return user
            self.user['by_username'][user.username].merge(user)
            mapper_user = self.user['by_username'][user.username]
            if mapper_user.free_company is not None:
                mapper_user_fc = self.fc['by_name'][mapper_user.free_company.name]
                if mapper_user not in mapper_user_fc.members:
                    mapper_user_fc.members.add(user)
        else:
            self.user_list.append(user)
            self.user['by_username'][user.username] = user
            if user.user_id:
                self.user['by_id'][user.user_id] = user
            if user.free_company:
                if user not in user.free_company.members:
                    user.free_company.members.add(user)
        return self.user['by_username'][user.username]

    def upsert_chat(self, chat: 'AetherChat') -> 'AetherChat':
        if chat.name in self.chat['by_name']:
            if chat is self.chat['by_name'][chat.name]:
                return chat
            self.chat['by_name'][chat.name].merge(chat)
        else:
            self.chat_list.append(chat)
            self.chat['by_name'][chat.name] = chat
            if chat.chat_id:
                self.chat['by_id'][chat.chat_id] = chat
        return self.chat['by_name'][chat.name]

    def upsert_fc(self, fc: 'AetherFreeCompany') -> 'AetherFreeCompany':
        if fc.name in self.fc['by_name']:
            if fc is self.fc['by_name'][fc.name]:
                return fc
            self.fc['by_name'][fc.name].merge(fc)
        else:
            self.fc_list.append(fc)
            self.fc['by_name'][fc.name] = fc
            if fc.fc_id:
                self.fc['by_id'][fc.fc_id] = fc
            if fc.tag:
                self.fc['by_tag'][fc.tag] = fc
        return self.fc['by_name'][fc.name]

    def upsert_any(self, aether_object: 'AetherDynamicObjectBase') -> 'AetherDynamicObjectBase':
        return_object = None
        if isinstance(aether_object, AetherChat):
            return_object = self.upsert_chat(aether_object)
        elif isinstance(aether_object, AetherUser):
            return_object = self.upsert_user(aether_object)
        elif isinstance(aether_object, AetherFreeCompany):
            return_object = self.upsert_fc(aether_object)
        return return_object


class AetherDynamicObjectBase(object):
    _ALWAYS_UPDATE = []
    _PRIMARY_KEY = 'name'

    def __hash__(self):
        return hash(getattr(self, self._PRIMARY_KEY)) \
               ^ (hash(self.__class__.__name__) ^ hash(getattr(self, self._PRIMARY_KEY)))

    def merge(self, other: AetherDynamicObjectMappings):
        if getattr(self, self._PRIMARY_KEY) != getattr(other, self._PRIMARY_KEY):
            logging.warning('Attempted to combine two different AetherDynamicObjects!')
            return
        for key in other.__dict__:
            if isinstance(self.__dict__[key], list):
                self.__dict__[key].extend(other.__dict__[key])
                continue
            if other.__dict__[key] and key in self._ALWAYS_UPDATE:
                if isinstance(self.__dict__[key], dict):
                    self.__dict__[key].update(other.__dict__[key])
                    continue
                setattr(self, key, other.__dict__[key])
                continue
            if not self.__dict__[key]:
                setattr(self, key, other.__dict__[key])


class AetherChat(AetherDynamicObjectBase):
    def __init__(
            self,
            name: str,
            chat_id: Optional[bytes] = None,
            members: Optional[Set['AetherUser']] = None
    ):
        self.name = name
        self.chat_id = chat_id
        if not members:
            members = set()
        self.members = members
        self.extras = {}

    def __str__(self):
        return_string = f'<AetherChat {self.name}'
        if self.chat_id:
            return_string += f' (ID:{self.chat_id})'
        if self.members:
            return_string += f', {len(self.members)} Members'
        return_string += '>'
        return return_string


class AetherFreeCompany(AetherDynamicObjectBase):
    def __init__(
            self,
            name: str,
            tag: Optional[str] = None,
            fc_id: Optional[bytes] = None,
            members: Optional[Set['AetherUser']] = None
    ):
        self.name = name
        self.tag = tag
        self.fc_id = fc_id
        if not members:
            members = set()
        self.members = members
        self.extras = {}

    def __str__(self):
        return_string = f'<{self.__class__.__name__} {self.name}'
        if self.tag:
            return_string += f' <<{self.tag}>>'
        if self.fc_id:
            return_string += f' (ID:{self.fc_id})'
        if self.members:
            return_string += f', {len(self.members)} Members'
        return_string += '>'
        return return_string

    def __len__(self):
        return len(self.members)


class AetherUser(AetherDynamicObjectBase):
    _ALWAYS_UPDATE = ['_user_class', '_level']
    _PRIMARY_KEY = 'username'

    def __init__(
            self,
            username: str,
            user_id: Optional[bytes] = None,
            companion_name: Optional[str] = None,
            server: Optional[str] = None,
            user_class: Optional[Union[str, bytes]] = None,
            level: Optional[Union[int, bytes]] = None,
            free_company: Optional[AetherFreeCompany] = None,
            submessage: Optional['AetherSubMessage'] = None
    ):
        self.username = username
        self.user_id = user_id
        self.companion_name = companion_name
        self.server = server
        self._user_class = None
        self.user_class = user_class
        self._level = None
        self.level = level
        self.free_company = free_company
        self.submessages = []
        if submessage:
            self.submessages.append(submessage)
        self.extras = {}

    def __str__(self):
        return_string = f'<{self.__class__.__name__} \'{self.username}\''
        if self.server:
            return_string += f'{self.server}]'
        if self.level:
            return_string += f' Lv. {self.level}'
        if self.user_class:
            return_string += f' {self.user_class}'
        return_string += '>'
        return return_string

    @property
    def user_class(self):
        return self._user_class

    @user_class.setter
    def user_class(self, value):
        if not value or value == bytes(1):
            return
        if isinstance(value, bytes):
            value = struct.unpack('B', value)[0]
        if isinstance(value, int):
            if value in FFXIV_CLASS_IDS:
                self._user_class = FFXIV_CLASS_IDS[value]
        if isinstance(value, str):
            self._user_class = value

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        if not value or value == bytes(1):
            return
        if isinstance(value, int):
            self._level = value
        elif isinstance(value, bytes):
            self._level = struct.unpack('B', value)[0]


class AetherParser(object):
    def __init__(
            self,
            pcap: Optional[str] = None,
            interface: Optional[str] = None,
            chat_only: bool = False,
            output_file: Optional[str] = None,
            scapy_pcap: bool = False,
            max_packets: int = 0,
            config: AetherConfig = AetherConfig()
    ):
        if pcap and interface:
            raise KeyError('Cannot specify both `pcap` and `interface`')
        self.pcap = pcap
        self.interface = interface
        self.chat_only = chat_only
        self.output_file = output_file
        self.scapy_pcap = scapy_pcap
        self.max_packets = max_packets
        self.config = config
        self.mapper = AetherDynamicObjectMappings(config=self.config)
        self.message_collection = AetherMessageCollection(mapper=self.mapper, config=self.config)
        self._ofp = None
        if self.output_file:
            self._ofp = open(self.output_file, 'wb+')

    def __del__(self):
        if self._ofp:
            self._ofp.close()

    @classmethod
    def from_json_file(cls, filename, **kwargs):
        self = cls(**kwargs)
        self.message_collection = AetherMessageCollection.from_file(
            filename,
            mapper=self.mapper,
            config=self.config
        )
        return self

    def parse(self):
        # Setting this to ignore un-needed `Unable to guess type` warning messages.
        logging.getLogger('scapy.runtime').setLevel(logging.CRITICAL)
        if self.interface:
            logging.debug(f'Listening on interface {self.interface}')
            sniff(
                iface=self.interface,
                filter='tcp portrange 55000-55555',
                count=self.max_packets,
                prn=self._parse_packet,
                store=False
            )
        if self.pcap and not self.scapy_pcap:
            self._parse_pcap()
        elif self.pcap and self.scapy_pcap:
            sniff(offline=self.pcap, count=self.max_packets, prn=self._parse_packet, store=False)
        self.mapper.cleanup()
        logging.info('Completed parsing')

    def _parse_pcap(self):
        capture = open(self.pcap, 'rb')
        pcap_format, byte_order = get_file_type(capture)
        packet_count = 0
        capture.seek(24)
        while True:
            if self.max_packets and packet_count >= self.max_packets:
                break
            ts = capture.read(4)
            ms = capture.read(4)
            sp = struct.pack('Q', capture.tell() - 8)
            try:
                length = struct.unpack(f'{byte_order}I', capture.read(4))[0]
            except struct.error:
                break
            packet_count += 1
            end_position = capture.tell() + 4 + length  # end pos
            capture.seek(16, 1)
            eth_type = capture.read(2)  # 16
            if eth_type == b'\x08\x00':  # IPv4
                ihl = ((struct.unpack('B', capture.read(1))[0] ^ 64) - 5) * 4  # get IHL
                capture.seek(8, 1)
                l2_proto = capture.read(1)
                capture.seek(2, 1)
                source_ip = capture.read(4)
                dest_ip = capture.read(4)
                capture.seek(ihl, 1)
                if l2_proto != b'\x06':
                    continue
                src_port = capture.read(2)
                dst_port = capture.read(2)
                capture.seek(8, 1)
                offset_byte = capture.read(1)
                extra_bytes = (((struct.unpack('B', offset_byte)[0] & 240) >> 4) - 5) * 4
                capture.seek(7, 1)
                capture.seek(extra_bytes, 1)
                data_length = end_position - capture.tell()
                packet_data = capture.read(data_length)
                if packet_data[0:16] != \
                        b'\x52\x52\xa0\x41\xff\x5d\x46\xe2' \
                        b'\x7f\x2a\x64\x4d\x7b\x99\xc4\x75':
                    continue
                self._parse_packet(packet_data)
                capture.seek(end_position)
        capture.close()

    def _parse_packet(self, pkt):
        if self.interface:
            try:
                data = pkt.getlayer(TCP).load
            except (TypeError, AttributeError):
                return
        elif self.pcap and not self.scapy_pcap:
            data = pkt
        else:
            raise NotImplementedError
        if data[0:16] != b'\x52\x52\xa0\x41\xff\x5d\x46\xe2\x7f\x2a\x64\x4d\x7b\x99\xc4\x75':
            return
        aether_message_bundle = AetherMessageBundle(
            data,
            self.message_collection,
            mapper=self.mapper
        )
        self.message_collection.add_bundle(aether_message_bundle, self.chat_only)
        if self._ofp:
            for message in aether_message_bundle.messages:
                for submessage in message.submessages:
                    if isinstance(submessage, AetherChatSubMessage):
                        logging.info(str(submessage))
                        self._ofp.write(bytes(str(submessage), 'utf-8'))
                        self._ofp.write(b'\n')
                        self._ofp.flush()


class AetherMessageCollection(object):
    def __init__(
            self,
            bundles: Optional[List['AetherMessageBundle']] = None,
            mapper: Optional['AetherDynamicObjectMappings'] = None,
            config: AetherConfig = AetherConfig()
    ):
        if not mapper:
            mapper = AetherDynamicObjectMappings()
        self.mapper = mapper
        self.config = config
        self.bundles = deque(maxlen=self.config.item_limits.bundles)
        self.messages = deque(maxlen=self.config.item_limits.messages)
        self.submessages = deque(maxlen=self.config.item_limits.submessages)
        self.chat_submessages = deque(maxlen=self.config.item_limits.chat_submessages)
        self.invalid_binaries = deque(maxlen=self.config.item_limits.invalid_binaries)
        self.broken_bundles = deque(maxlen=self.config.item_limits.broken_bundles)
        self.decompression_errors = deque(maxlen=self.config.item_limits.decompression_errors)
        self.chat_submessages_by_type = \
            LimitedDict(
                default_factory=lambda: deque(
                    maxlen=self.config.item_limits.chat_submessages_by_type
                )
            )
        self.submessages_by_type = LimitedDict(
            default_factory=lambda: deque(maxlen=self.config.item_limits.submessages_by_type)
        )
        self.strings_by_type = LimitedDict(
            default_factory=lambda: deque(maxlen=self.config.item_limits.strings_by_type)
        )
        if bundles:
            for bundle in bundles:
                self.add_bundle(bundle)

    def __str__(self):
        return f'<{self.__class__.__name__} ({len(self)} messages)>'

    def __len__(self):
        return len(self.messages)

    def _to_dict(self):
        collection_object = {
            'bundles': []
        }
        for bundle in self.bundles:
            collection_object['bundles'].append(bundle.to_b64())
        return collection_object

    def to_json(self):
        return json.dumps(self._to_dict())

    def to_file(self, filename):
        with gzip.open(filename, 'wt') as jfp:
            json.dump(self._to_dict(), jfp)

    @classmethod
    def from_json(cls, json_str):
        pass

    @classmethod
    def from_file(
            cls,
            filename: str,
            mapper: Optional[AetherDynamicObjectMappings] = None,
            config: Optional[AetherConfig] = None
    ) -> 'AetherMessageCollection':
        with gzip.open(filename, 'rt') as jfp:
            collection_object = json.load(jfp)
        self = cls(mapper=mapper, config=config)
        for bundle_b64 in collection_object['bundles']:
            self.add_bundle(
                AetherMessageBundle.from_b64(
                    bundle_b64,
                    parent_collection=self,
                    mapper=mapper
                )
            )
        return self

    def search_submessages(
            self,
            search: Optional[Union[str, bytes]] = None
    ) -> Set['AetherSubMessage']:
        if isinstance(search, str):
            search = search.encode()
        return set([
            submessage for submessage in self.submessages
            if search in submessage.chunks['Submessage Body']
        ])

    def filter_submessages(
            self,
            **kwargs
    ) -> Set['AetherSubMessage']:
        return_set = set()
        for (key, value) in kwargs:
            for submessage in self.submessages:
                if getattr(submessage, key, None) == value:
                    return_set.add(submessage)
        return return_set

    def add_bundle(self, message_bundle: 'AetherMessageBundle', only_chat: bool = False):
        if not only_chat:
            self.bundles.append(message_bundle)
            if not message_bundle.parent_collection:
                message_bundle.parent_collection = self
            for message in message_bundle.messages:
                self.messages.append(message)
                if message.decompression_error:
                    self.decompression_errors.append(message)
                for submessage in message.submessages:
                    self.submessages.append(submessage)
                    try:
                        self.submessages_by_type[struct.unpack(
                            '<H', submessage.chunks['Submessage Type']
                        )[0]].append(submessage)
                        self.strings_by_type[struct.unpack(
                            '<H', submessage.chunks['Submessage Type']
                        )[0]].append(submessage.strings)
                    except struct.error:
                        self.submessages_by_type[65535].append(submessage)
                        self.strings_by_type[65535].append(submessage.strings)
                    except KeyError:
                        self.submessages_by_type[65536].append(submessage)
                        self.strings_by_type[65536].append(submessage.strings)
                    if isinstance(submessage, AetherChatSubMessage):
                        self.chat_submessages.append(submessage)
                        self.chat_submessages_by_type[submessage.chat_type].append(submessage)
                    new_aether_objects = []
                    for aether_object in submessage.mappings:
                        new_aether_objects.append(self.mapper.upsert_any(aether_object))
                    submessage.mappings = new_aether_objects
        else:
            chat_bundle = False
            for message in message_bundle.messages:
                chat_message = False
                for submessage in message.submessages:
                    new_aether_objects = []
                    for aether_object in submessage.mappings:
                        new_aether_objects.append(self.mapper.upsert_any(aether_object))
                    submessage.mappings = new_aether_objects
                    if isinstance(submessage, AetherChatSubMessage):
                        chat_message = True
                        self.submessages.append(submessage)
                        self.chat_submessages.append(submessage)
                        self.chat_submessages_by_type[submessage.chat_type].append(submessage)
                        self.submessages_by_type[struct.unpack(
                            '<H', submessage.chunks['Submessage Type']
                        )[0]].append(submessage)
                    elif isinstance(submessage, AetherChatMappingSubMessage):
                        chat_message = True
                        self.submessages.append(submessage)
                        self.submessages_by_type[struct.unpack(
                            '<H', submessage.chunks['Submessage Type']
                        )[0]].append(submessage)
                    else:
                        break
                if chat_message:
                    self.messages.append(message)
                    chat_bundle = True
            if chat_bundle:
                if not message_bundle.parent_collection:
                    message_bundle.parent_collection = self
                self.bundles.append(message_bundle)


class AetherMessageBundle(object):
    def __init__(
            self,
            binary_data,
            parent_collection: Optional[AetherMessageCollection] = None,
            mapper: Optional[AetherDynamicObjectMappings] = None
    ):
        self.binary_data = binary_data
        self.parent_collection = parent_collection
        if not mapper:
            mapper = AetherDynamicObjectMappings()
        self.mapper = mapper
        self.messages = []
        self.submessages = []
        self.has_invalid_messages = False
        self._parse_binary()

    def __str__(self):
        return f'<{self.__class__.__name__} ({len(self)} message(s))>'

    def __len__(self):
        return len(self.messages)

    def _parse_binary(self):
        if self.binary_data[0:16] != b'\x52\x52\xa0\x41\xff\x5d\x46\xe2' \
                                     b'\x7f\x2a\x64\x4d\x7b\x99\xc4\x75':
            raise ValueError(f'Binary data does not look like a FFXIV protocol object!\n'
                             f'Dump: {self.binary_data}')
        message_offset = 0
        partial_message = False
        while True:
            try:
                message_length = struct.unpack(
                    '<I', self.binary_data[message_offset + 24:message_offset + 28]
                )[0]
            except struct.error:
                break
            binary_message = self.binary_data[message_offset:message_offset + message_length]
            if len(binary_message) != message_length:
                logging.debug(
                    f'Length mismatch (expected {message_length}, got {len(binary_message)})'
                )
                partial_message = True
            if len(binary_message) == 0:
                logging.debug('Discarding 0-length message')
                break
            try:
                message = AetherMessage(binary_message, parent_bundle=self, mapper=self.mapper)
            except ValueError:
                self.parent_collection.invalid_binaries.append(binary_message)
                self.parent_collection.broken_bundles.append(self)
                self.has_invalid_messages = True
                break
            message.partial_message = partial_message
            self.messages.append(message)
            for submessage in message.submessages:
                self.submessages.append(submessage)
            if partial_message:
                break
            message_offset = message_offset + message_length

    def to_b64(self):
        return base64.b64encode(self.binary_data).decode()

    @classmethod
    def from_b64(cls, base64_data, **kwargs):
        return cls(binary_data=base64.b64decode(base64_data), **kwargs)


class AetherMessageBase(object):
    def __init__(
            self,
            binary_data,
            parent_bundle: Optional[AetherMessageBundle] = None,
            mapper: Optional[AetherDynamicObjectMappings] = None
    ):
        self.binary = self._normalize(binary_data)
        self.parent_bundle = parent_bundle
        if not mapper:
            mapper = AetherDynamicObjectMappings()
        self.mapper = mapper
        self.parent_collection = None
        if self.parent_bundle:
            self.parent_collection = self.parent_bundle.parent_collection
        self.binary_header = None
        self.binary_body = None
        self.chunks = OrderedDict()
        self.timestamp = None
        self.compressed = False
        self.decompression_error = False
        self.partial_message = False
        self.malformed_message = False
        self.message_length = 0
        self.raw_message_length = 0
        self.submessages = []
        self._parse_binary()

    def __str__(self):
        return f'<{self.__class__.__name__} ' \
               f'({len(self)} bytes, {len(self.submessages)} submessages)>'

    def __len__(self):
        return self.message_length

    def hexdump(self):
        for chunk in self.chunks:
            print(f'{chunk}\n{"-" * len(chunk)}\n{hexdump(self.chunks[chunk], result="return")}\n')

    @classmethod
    def from_hex(cls, hex_data):
        return cls(dehex(hex_data))

    @staticmethod
    def _normalize(data):
        if isinstance(data, str):
            data = dehex(data)
        if not isinstance(data, bytes):
            raise ValueError('Data provided must be bytes or string of hex!')
        if data[0:16] != b'\x52\x52\xa0\x41\xff\x5d\x46\xe2\x7f\x2a\x64\x4d\x7b\x99\xc4\x75':
            raise ValueError(f'Binary data does not look like a FFXIV protocol object!\n'
                             f'Dump: {data}')
        return data

    def _parse_binary(self):
        raise NotImplementedError


class AetherMessage(AetherMessageBase):
    def _parse_binary(self):
        self.binary_header = self.binary[0:40]
        self.binary_body = self.binary[40:]
        self.chunks['Protocol Magic'] = self.binary[0:16]
        self.chunks['Timestamp'] = self.binary[16:24]
        try:
            self.timestamp = datetime.fromtimestamp(
                float(struct.unpack('<Q', self.chunks['Timestamp'])[0]) / 1000
            )
            self.chunks['Message Length'] = self.binary[24:28]
            self.raw_message_length = struct.unpack('<I', self.chunks['Message Length'])[0]
            self.chunks['Connection Type'] = self.binary[28:30]
            self.chunks['Message Count'] = self.binary[30:32]
            self.message_count = struct.unpack('<H', self.chunks['Message Count'])[0]
            self.chunks['Compression'] = self.binary[33:34]
            self.compressed = struct.unpack('?', self.chunks['Compression'])[0]
        except struct.error:
            self.malformed_message = True
        self.chunks['Unknown 2'] = self.binary[34:40]
        self.chunks['Raw Message'] = self.binary[40:]
        self.chunks['Message Body'] = self.chunks['Raw Message']
        if self.compressed:
            decompressor = zlib.decompressobj()
            try:
                self.chunks['Uncompressed Message'] = decompressor.decompress(
                    self.chunks['Raw Message']
                )
                self.chunks['Message Body'] = self.chunks['Uncompressed Message']
            except zlib.error:
                self.decompression_error = True
                return
        self.message_length = 40 + len(self.chunks['Message Body'])
        # self.strings = get_strings(self.chunks['Message Body'], 7)
        partial_submessage = False
        offset = 0
        while True:
            try:
                length = struct.unpack(
                    '<I', self.chunks['Message Body'][offset:offset + 4]
                )[0]
            except struct.error:
                break
            binary_message = self.chunks['Message Body'][offset:offset + length]
            if len(binary_message) != length:
                logging.debug(f'Length mismatch (expected {length}, got {len(binary_message)})')
                partial_submessage = True
            if len(binary_message) == 0:
                logging.debug('Discarding 0-length submessage')
                break
            submessage = AetherSubMessage(binary_message, parent_message=self, mapper=self.mapper)
            submessage.partial_submessage = partial_submessage
            self.submessages.append(submessage)
            if partial_submessage:
                break
            offset = offset + length


class AetherSubMessageBase(object):
    def __init__(
            self,
            binary_message,
            parent_message: Optional[AetherMessageBase] = None,
            mapper: AetherDynamicObjectMappings = AetherDynamicObjectMappings()
    ):
        self.binary = binary_message
        self.parent_message = parent_message
        self.mapper = mapper
        self.mappings = []
        self.length = 0
        self.seconds = 0
        self.type_int = 0
        self.parent_bundle = None
        self.parent_collection = None
        if self.parent_message:
            self.parent_bundle = self.parent_message.parent_bundle
        """ This code never works and I don't know why??????
        if self.parent_bundle:
            self.parent_collection = self.parent_message.parent_bundle.parent_collection
        """
        self.chunks = OrderedDict()
        self._parse_binary()

    def __str__(self):
        return f'<{self.__class__.__name__} ({len(self)} bytes)>'

    def __len__(self):
        return self.length

    def _parse_binary(self):
        raise NotImplementedError

    def hexdump(self):
        for chunk in self.chunks:
            print(f'{chunk}\n{"-" * len(chunk)}\n{hexdump(self.chunks[chunk], result="return")}\n')


class AetherSubMessage(AetherSubMessageBase):
    def __new__(cls, data, parent_message=None, mapper=None):
        if cls is AetherSubMessage:
            cls = cls._determine_submessage_type(data)
        self = cls._from_binary()
        return self

    @classmethod
    def _from_binary(cls):
        self = object.__new__(cls)
        return self

    @staticmethod
    def _determine_submessage_type(binary_data):
        if binary_data[12:14] == b'\x03\x00':
            try:
                type_int = struct.unpack('<H', binary_data[18:20])[0]
            except struct.error:
                type_int = 65535
            if type_int in FFXIV_SUBMESSAGE_TO_CLASS:
                return FFXIV_SUBMESSAGE_TO_CLASS[type_int]
        return AetherUnknownSubMessage

    def _parse_binary(self):
        self.chunks['Submessage Length'] = self.binary[0:4]
        try:
            self.length = struct.unpack('<I', self.chunks['Submessage Length'])[0]
        except struct.error:
            pass
        self.chunks['Source Actor'] = self.binary[4:8]
        self.chunks['Target Actor'] = self.binary[8:12]
        self.chunks['Segment Type'] = self.binary[12:14]
        if self.chunks['Segment Type'] == b'\x03\x00':
            self.chunks['Submessage Type'] = self.binary[18:20]
            try:
                self.type_int = struct.unpack('<H', self.chunks["Submessage Type"])[0]
            except struct.error:
                self.type_int = 65535
            self.chunks['Server ID'] = self.binary[22:24]
            self.chunks['Submessage Seconds'] = self.binary[24:28]
            try:
                self.seconds = struct.unpack('<I', self.chunks['Submessage Seconds'])[0]
            except struct.error:
                pass
            if self.seconds > 1570000000:
                self.timestamp = datetime.fromtimestamp(self.seconds)
            else:
                self.timestamp = self.parent_message.timestamp
            self.chunks['Submessage Body'] = self.binary[32:]
        else:
            self.chunks['Submessage Body'] = self.binary[16:]
        self.strings = []
        if self.mapper.config.extract_strings:
            self.strings = get_strings(self.chunks['Submessage Body'], 5)


class AetherChatSubMessage(AetherSubMessage):
    CHAT_FORMAT = {
        'type': {
            'Emote': '[{self.timestamp:{ts}}] ({self.chat_identifier}) {self.chat_sender} '
                     '{self.chat_message}',
            'Console': '[{self.timestamp:{ts}}] ({self.chat_identifier}) {self.chat_message}',
            'FC Announcement': '[{self.timestamp:{ts}}] '
                               '({self.chat_identifier}) {self.chat_message}',
        },
        'default': '[{self.timestamp:{ts}}] ({self.chat_identifier}) {self.chat_sender}: '
                   '{self.chat_message}',
        'chat_identifier': '{self.chat_type} [{self.chat_name}]',
        'timestamp': '%Y-%m-%d %H:%M:%S'
    }

    def __init__(
            self,
            binary_data,
            **kwargs
    ):
        self.chat_sender = None
        self.chat_message = None
        self.chat_type = 'Unknown'
        self.chat_name = None
        self.real_chat = False
        self.self_chat = False
        self.timestamp = None
        self.mappings = []
        self.message_extras = {}
        super().__init__(binary_data, **kwargs)
        logging.info(str(self))

    def __str__(self):
        if self.chat_type in self.CHAT_FORMAT['type']:
            return self.CHAT_FORMAT['type'][self.chat_type].format(
                self=self,
                ts=self.CHAT_FORMAT['timestamp']
            )
        else:
            return self.CHAT_FORMAT['default'].format(self=self, ts=self.CHAT_FORMAT['timestamp'])

    @property
    def chat_identifier(self):
        if self.chat_name:
            return self.CHAT_FORMAT['chat_identifier'].format(self=self)
        else:
            return self.chat_type

    @staticmethod
    def _parse_chat_sugar(sugar_bytes):
        sugar_type = 'unknown'
        extras = {'binary': sugar_bytes}
        if sugar_bytes[3:5] == b'\xc9\x02':
            sugar_type = 'user'
            extras['server_byte'] = sugar_bytes[5]
            extras['user_name'] = sugar_bytes[8:-1]
            extras['server'] = None
            if extras['server_byte'] in FFXIV_SERVER_IDS:
                extras['server'] = FFXIV_SERVER_IDS[extras['server_byte']]
                replacement = b'\xf0\x9f\x91\xa4{' + \
                              extras['user_name'] + b'[' + bytes(extras['server'], 'utf-8') + b']}'
            else:
                replacement = b'\xf0\x9f\x91\xa4{' \
                              + extras['user_name'] \
                              + b'[' \
                              + bytes(str(extras['server_byte']), 'utf-8') \
                              + b']}'
        elif sugar_bytes[3:5] == b'\xc9\x04':
            sugar_type = 'location'
            replacement = b'\xf0\x9f\x93\x8c{' + bytes(str(sugar_bytes), 'utf-8') + b'}'
        elif sugar_bytes[3:5] == b'\xc9\x05':
            sugar_type = 'item'
            extras['item_id'] = struct.unpack('>H', sugar_bytes[6:8])[0]
            extras['item_name'] = sugar_bytes[13:-1].decode()
            replacement = b'\xf0\x9f\x93\xa6{[' \
                          + bytes(str(extras['item_id']), 'utf-8') \
                          + b'] ' \
                          + bytes(extras['item_name'], 'utf-8') \
                          + b'}'
        elif sugar_bytes[3:5] == b'\xc9\x06':
            sugar_type = 'sound'
            extras['sound_id'] = struct.unpack('B', sugar_bytes[5:6])[0]
            replacement = b'\xf0\x9f\x94\x94{' + bytes(str(extras['sound_id']), 'utf-8') + b'}'
        elif sugar_bytes[3:5] == b'8\xae':
            sugar_type = 'skill'
            replacement = b'\xf0\x9f\xa4\xb9{' + bytes(str(sugar_bytes), 'utf-8') + b'}'
        elif sugar_bytes[3:5] == b'9\xf6':
            sugar_type = 'action'
            replacement = b'\xf0\x9f\x8f\x83{' + bytes(str(sugar_bytes), 'utf-8') + b'}'
        else:
            replacement = b'\xe2\x9d\x93{' + bytes(str(sugar_bytes), 'utf-8') + b'}'
        return replacement, sugar_type, extras

    @staticmethod
    def _parse_chat_message(message_bytes: bytes):
        message_extras = LimitedDict(default_factory=list)
        text_message = '❌{Message Not Parsable}'
        offset = 0
        if b'\x02\x2e' in message_bytes:
            while True:
                if message_bytes[offset:offset + 1] == b'':
                    break
                if message_bytes[offset:offset + 2] == b'\x02\x2e':
                    sugar_length = struct.unpack('B', message_bytes[offset + 2:offset + 3])[0]
                    sugar_bytes = message_bytes[offset:offset + 3 + sugar_length]
                    replacement, sugar_type, extras = \
                        AetherChatSubMessage._parse_chat_sugar(sugar_bytes)
                    message_bytes = message_bytes.replace(sugar_bytes, replacement)
                    message_extras[sugar_type].append(extras)
                    offset = len(replacement) + offset
                    continue
                offset += 1
        """
        item_matches = re.findall(
            b'\x02\x2e.\xc9\x05\xf2(..)\x02\x01\x01\xff.([^\x03]*)\x03', message_bytes, flags=re.S
        )
        user_matches = re.findall(
            b'\x02\x2e.\xc9\x02(.)\xff.([^\x03]*)\x03', message_bytes, flags=re.S
        )
        location_matches = re.findall(
            b'\x02\x2e.\xc9\x04\xf2([^\x03]*)\x03', message_bytes, flags=re.S
        )
        action_matches = re.findall(b'\x02\x2e.9\xf6([^\x03]*)\x03', message_bytes, flags=re.S)
        sound_matches = re.findall(b'\x02\x2e.\xc9\x06([^\x03]*)\x03', message_bytes, flags=re.S)
        skill_matches = re.findall(b'\x02\x2e.8\xae([^\x03]*)\x03', message_bytes, flags=re.S)
        """
        try:
            text_message = message_bytes.rstrip(b'\x00').decode()
        except UnicodeDecodeError:
            pass
        return text_message, message_extras

    def _parse_binary(self):
        super()._parse_binary()
        if self.chunks['Submessage Type'] == b'e\x00':
            # self.chunks['Chat Length'] = self.chunks['Submessage Body'][0:4]
            self.chunks['Chat ID'] = self.chunks['Submessage Body'][0:6]
            self.chunks['Chat Type'] = self.chunks['Submessage Body'][4:6]
            self.chunks['Self Marker'] = self.chunks['Submessage Body'][14:15]
            if self.chunks['Chat Type'] in FFXIV_CHAT_X:
                self.chat_type = FFXIV_CHAT_X[self.chunks['Chat Type']]
            if self.chunks['Chat ID'] in self.mapper.chat['by_id']:
                self.chat_name = self.mapper.chat['by_id'][self.chunks['Chat ID']].name
            if self.chunks['Self Marker'] != b'@':
                self.self_chat = True
                self.chat_sender = 'You'
                self.chunks['Chat Message'] = self.chunks['Submessage Body'][8:]
            else:
                self.chunks['User Unique 1'] = self.chunks['Submessage Body'][8:15]
                self.chunks['User ID'] = self.chunks['Submessage Body'][16:20]
                self.chunks['User Name'] = self.chunks['Submessage Body'][23:55]
                try:
                    self.chat_sender = self.chunks['User Name'].rstrip(b'\x00').decode()
                except UnicodeDecodeError:
                    pass
                self.mappings.append(AetherUser(
                    username=self.chat_sender, user_id=self.chunks['User ID'], submessage=self
                ))
                self.chunks['Chat Message'] = self.chunks['Submessage Body'][55:]
        elif self.chunks['Submessage Type'] in (b'\xdc\x02', b'H\x03', b'\x4b\x01'):
            self.chunks['Chat Type'] = self.chunks['Submessage Body'][14:15]
            if self.chunks['Chat Type'] in FFXIV_CHAT_P:
                self.chat_type = FFXIV_CHAT_P[self.chunks['Chat Type']]
            self.chunks['User ID'] = self.chunks['Submessage Body'][8:12]
            self.chunks['User Name'] = self.chunks['Submessage Body'][16:48]
            try:
                self.chat_sender = self.chunks['User Name'].rstrip(b'\x00').decode()
            except UnicodeDecodeError:
                pass
            self.mappings.append(AetherUser(
                username=self.chat_sender, user_id=self.chunks['User ID'], submessage=self
            ))
            self.chunks['Chat Message'] = self.chunks['Submessage Body'][48:]
        elif self.chunks['Submessage Type'] == b'\x15\x03':
            self.chat_type = 'Console'
            self.chunks['Chat Message'] = self.chunks['Submessage Body'][1:]
        elif self.chunks['Submessage Type'] == b')\x03':
            self.chat_type = 'FC Announcement'
            self.chunks['Chat Message'] = self.chunks['Submessage Body'][1:]
        else:
            logging.debug(self.chunks['Submessage Type'])
        self.chat_message, self.message_extras = \
            self._parse_chat_message(self.chunks['Chat Message'])


class AetherChatMappingSubMessage(AetherSubMessage):
    def __init__(self, binary_data, **kwargs):
        self.mappings = []
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        if self.type_int == 847:
            offset = 8
            definition = 1
            while True:
                chat_id = self.chunks['Submessage Body'][offset:offset + 6]
                chat_name = self.chunks['Submessage Body'][offset + 12:offset + 44]
                if len(chat_id) != 6 or len(chat_name) != 32 or chat_name == bytes(32):
                    break
                self.chunks[f'Chat Definition {definition}'] = \
                    self.chunks['Submessage Body'][offset:offset + 44]
                chat_object = AetherChat(name=chat_name.rstrip(b'\x00').decode(), chat_id=chat_id)
                self.mappings.append(chat_object)
                definition += 1
                offset += 56
        elif self.type_int == 105:
            offset = 0
            definition = 1
            while True:
                chat_id = self.chunks['Submessage Body'][offset:offset + 6]
                chat_name = self.chunks['Submessage Body'][offset + 9:offset + 41]
                if len(chat_id) != 6 or len(chat_name) != 32 or chat_name == bytes(32):
                    break
                self.chunks[f'Chat Definition {definition}'] = \
                    self.chunks['Submessage Body'][offset:offset + 41]
                chat_object = AetherChat(name=chat_name.rstrip(b'\x00').decode(), chat_id=chat_id)
                self.mappings.append(chat_object)
                definition += 1
                offset += 48


class AetherFCMembershipSubMessage(AetherSubMessage):
    def __init__(self, binary_data, **kwargs):
        self.mappings = []
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        self.chunks['Unknown'] = self.chunks['Submessage Body'][0:26]
        self.chunks[f'FC Name'] = self.chunks['Submessage Body'][26:72]
        self.chunks[f'Username'] = self.chunks['Submessage Body'][72:104]
        username = self.chunks[f'Username'].rstrip(b'\x00').decode()
        free_company_name = self.chunks[f'FC Name'].rstrip(b'\x00').decode()
        if free_company_name in self.mapper.fc['by_name']:
            free_company_object = self.mapper.fc['by_name'][free_company_name]
        else:
            free_company_object = AetherFreeCompany(name=free_company_name)
        self.mappings.append(free_company_object)
        user_object = AetherUser(
            username=username,
            free_company=free_company_object,
            submessage=self
        )
        self.mappings.append(user_object)


class AetherUserFCMappingSubMessage(AetherSubMessage):
    def __init__(self, binary_data, **kwargs):
        self.mappings = []
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        offset = 16
        definition = 1
        while True:
            if self.chunks['Submessage Body'][offset:offset + 8] == bytes(8):
                break
            if len(self.chunks['Submessage Body'][offset:offset + 88]) != 88:
                break
            self.chunks[f'Content ID {definition}'] = \
                self.chunks['Submessage Body'][offset:offset + 8]
            self.chunks[f'Zone ID {definition}'] = \
                self.chunks['Submessage Body'][offset + 20:offset + 22]
            self.chunks[f'Zone ID1 {definition}'] = \
                self.chunks['Submessage Body'][offset + 22:offset + 24]
            self.chunks[f'Status Mask {definition}'] = \
                self.chunks['Submessage Body'][offset + 32:offset + 40]
            self.chunks[f'Class {definition}'] = \
                self.chunks['Submessage Body'][offset + 40:offset + 41]
            self.chunks[f'Level {definition}'] = \
                self.chunks['Submessage Body'][offset + 42:offset + 43]
            self.chunks[f'Username {definition}'] = \
                self.chunks['Submessage Body'][offset + 47:offset + 79]
            self.chunks[f'FC Tag {definition}'] = \
                self.chunks['Submessage Body'][offset + 79:offset + 88]
            self.chunks[f'User Definition {definition}'] = \
                self.chunks['Submessage Body'][offset:offset + 88]
            username = self.chunks[f'Username {definition}'].rstrip(b'\x00').decode()
            free_company_tag = self.chunks[f'FC Tag {definition}'].rstrip(b'\x00').decode()
            free_company_object = None
            if free_company_tag in self.mapper.fc['by_tag']:
                free_company_object = self.mapper.fc['by_tag'][free_company_tag]
            user_object = AetherUser(
                username=username,
                user_class=self.chunks[f'Class {definition}'],
                level=self.chunks[f'Level {definition}'],
                free_company=free_company_object,
                submessage=self
            )
            self.mappings.append(user_object)
            definition += 1
            offset += 88


class AetherUserIntroductionSubMessage(AetherSubMessage):
    def __init__(self, binary_data, **kwargs):
        self.mappings = []
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        offset = 513
        if len(self.chunks['Submessage Body'][offset:offset + 88]) != 88:
            return
        self.chunks[f'Level'] = self.chunks['Submessage Body'][129:130]
        self.chunks[f'Class'] = self.chunks['Submessage Body'][130:131]
        self.chunks[f'Username'] = self.chunks['Submessage Body'][560:592]
        self.chunks[f'FC Tag'] = self.chunks['Submessage Body'][618:627]
        username = self.chunks[f'Username'].rstrip(b'\x00').decode()
        free_company_tag = self.chunks[f'FC Tag'].rstrip(b'\x00').decode()
        free_company_object = None
        if free_company_tag in self.mapper.fc['by_tag']:
            free_company_object = self.mapper.fc['by_tag'][free_company_tag]
        user_object = AetherUser(
            username=username,
            user_class=self.chunks[f'Class'],
            level=self.chunks[f'Level'],
            free_company=free_company_object,
            submessage=self
        )
        self.mappings.append(user_object)


class AetherAllianceMappingSubMessage(AetherSubMessage):
    def __init__(self, binary_data, **kwargs):
        self.mappings = []
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        offset = 0
        definition = 1
        while True:
            if self.chunks['Submessage Body'][offset:offset + 2] == b'\x02\x00':
                break
            if len(self.chunks['Submessage Body'][offset:offset + 48]) != 48:
                break
            self.chunks[f'User Definition {definition}'] = \
                self.chunks['Submessage Body'][offset:offset + 48]
            self.chunks[f'User Name {definition}'] = \
                self.chunks['Submessage Body'][offset:offset + 32]
            self.chunks[f'User ID {definition}'] = \
                self.chunks['Submessage Body'][offset + 32:offset + 36]
            self.chunks[f'User Unknown1 {definition}'] = \
                self.chunks['Submessage Body'][offset + 36:offset + 40]
            self.chunks[f'User Unknown2 {definition}'] = \
                self.chunks['Submessage Body'][offset + 40:offset + 44]
            self.chunks[f'User Class {definition}'] = \
                self.chunks['Submessage Body'][offset + 44:offset + 45]
            self.chunks[f'User Level {definition}'] = \
                self.chunks['Submessage Body'][offset + 45:offset + 46]
            self.chunks[f'User Unknown3 {definition}'] = \
                self.chunks['Submessage Body'][offset + 44:offset + 48]
            username = self.chunks[f'User Name {definition}'].rstrip(b'\x00').decode()
            self.mappings.append(
                AetherUser(
                    username=username,
                    user_id=self.chunks[f'User ID {definition}'],
                    user_class=self.chunks[f'User Class {definition}'],
                    level=self.chunks[f'User Level {definition}'],
                    submessage=self
                )
            )
            definition += 1
            offset += 48
        self.chunks['Unknown Remainder'] = self.chunks['Submessage Body'][offset:]


class AetherServerDefinitionSubMessage(AetherSubMessage):
    """ Advertises three values -- one unknown, a server ID, and server name. """


class AetherFellowshipListSubMessage(AetherSubMessage):
    pass


class AetherFCRankDefinitionSubMessage(AetherSubMessage):
    pass


class AetherUnknownSubMessage(AetherSubMessage):
    def __init__(self, binary_data, **kwargs):
        super().__init__(binary_data, **kwargs)

    def __str__(self):
        return f'<class {self.__class__.__name__} Type {self.type_int} ({len(self)} bytes)>'

    def _parse_binary(self):
        super()._parse_binary()
        self.chunks['Unknown'] = self.binary[28:]


class AetherUnknownMessage(AetherMessage):
    def __init__(self, binary_data, **kwargs):
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        self.chunks['Unknown'] = self.binary[26:]


class AetherUserIntroduction(AetherMessage):
    def __init__(self, binary_data, **kwargs):
        super().__init__(binary_data, **kwargs)

    def _parse_binary(self):
        super()._parse_binary()
        self.chunks['Unknown'] = self.binary[26:]


FFXIV_SUBMESSAGE_TO_CLASS = {
    732: AetherChatSubMessage,
    101: AetherChatSubMessage,
    789: AetherChatSubMessage,
    809: AetherChatSubMessage,
    840: AetherChatSubMessage,
    331: AetherChatSubMessage,
    902: AetherUserIntroductionSubMessage,
    579: AetherUserIntroductionSubMessage,
    105: AetherChatMappingSubMessage,
    464: AetherAllianceMappingSubMessage,
    627: AetherAllianceMappingSubMessage,
    122: AetherUserFCMappingSubMessage,
    666: AetherUserFCMappingSubMessage,
    300: AetherFCMembershipSubMessage,
    854: AetherServerDefinitionSubMessage,
    756: AetherFellowshipListSubMessage,
    847: AetherChatMappingSubMessage,
    308: AetherFCRankDefinitionSubMessage,

    258: AetherUnknownSubMessage,
    861: AetherUnknownSubMessage,
    555: AetherUnknownSubMessage,
    192: AetherUnknownSubMessage,
    211: AetherUnknownSubMessage,
    770: AetherUnknownSubMessage
}
