#!/usr/bin/env python
# -*- coding: utf-8 -*-
from aether_parser.classes import AetherParser
from aether_parser.utils import get_options


def main():
    options = get_options()
    parser = AetherParser(**options)
    parser.parse()


if __name__ == '__main__':
    main()
