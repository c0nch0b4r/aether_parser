#!/usr/bin/env python
# -*- coding: utf-8 -*-

"[46:47]"
FFXIV_CHAT_P = {
    b'\n': 'Say',
    b'\x0b': 'Shout',
    b'\x0f': 'Alliance',
    b'\x1c': 'Emote',
    b'\x1e': 'Yell'
}

"[32:34]"
FFXIV_CHAT_X = {
    b'\x03\x00': 'Free Company',
    b'\x02\x00': 'Link Shell',
    b'\x01\x00': 'Party'
}

# Adapted from: https://github.com/SapphireServer/Sapphire/pull/571
FFXIV_SERVER_IDS = {
    35: 'Brynhildr',
    36: 'Famfrit',
    38: 'Mateus',
    40: 'Omega',
    41: 'Jenova',
    42: 'Zalera',
    50: 'Kujata',
    54: 'Exodus',
    55: 'Faerie',
    56: 'Lamia',
    58: 'Siren',
    63: 'Diabolos',
    64: 'Gilgamesh',
    65: 'Leviathan',
    66: 'Midgardsormr',
    69: 'Atomos',
    72: 'Moogle',
    73: 'Tonberry',
    74: 'Adamantoise',
    75: 'Coeurl',
    76: 'Malboro',
    78: 'Ultros',
    79: 'Behemoth',
    81: 'Cerberus',
    82: 'Goblin',
    84: 'Louisoix',
    86: 'Spriggan',
    92: 'Balmung',
    94: 'Excalibur',
    96: 'Hyperion',
    98: 'Ragnarok',
    100: 'Sargatanas'
}

# Might be: https://github.com/SapphireServer/Sapphire/blob/002850167b70fd83b4d04536d1f0c5e4e3ece315/src/tools/exd_common_gen/generated.h#L58 ?
FFXIV_CLASS_IDS = {
    0: 'Adventurer',
    1: 'Gladiator',
    2: 'Pugilist',
    3: 'Marauder',
    4: 'Lancer',
    5: 'Archer',
    6: 'Conjurer',
    7: 'Thaumaturge',
    8: 'Carpenter',
    9: 'Blacksmith',
    10: 'Armorer',
    11: 'Goldsmith',
    12: 'Leatherworker',
    13: 'Weaver',
    14: 'Alchemist',
    15: 'Culinarian',
    16: 'Miner',
    17: 'Botanist',
    18: 'Fisher',
    19: 'Paladin',
    20: 'Monk',
    21: 'Warrior',
    22: 'Dragoon',
    23: 'Bard',
    24: 'Whitemage',
    25: 'Blackmage',
    26: 'Arcanist',
    27: 'Summoner',
    28: 'Scholar',
    29: 'Rogue',
    30: 'Ninja',
    31: 'Machinist',
    32: 'Darkknight',
    33: 'Astrologian',
    34: 'Samurai',
    35: 'Redmage',
    37: 'Gunbreaker'
}

FILE_MAGIC_TO_TYPE = {
    b'\xA1\xB2\xC3\xD4': ('pcap', '>'),
    b'\xD4\xC3\xB2\xA1': ('pcap', '<'),
    b'\xA1\xB2\x3C\x4D': ('pcap', '>'),
    b'\x4D\x3C\xB2\xA1': ('pcap', '<'),
    b'\x0A\x0D\x0D\x0A': ('pcapng', '')
}
